export interface IUserLoginDetails {
    email: string,
    password: string
}
