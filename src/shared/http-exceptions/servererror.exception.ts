import { HttpException, HttpStatus } from '@nestjs/common';

export class ServerErrorException extends HttpException {
    constructor(msg?: string) {
        super({
            statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
            error: msg || 'Internal Server Error',
            timestamp: new Date().toISOString()
        }, HttpStatus.INTERNAL_SERVER_ERROR)
    }
}
