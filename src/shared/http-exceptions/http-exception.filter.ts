import { ArgumentsHost, Catch, ExceptionFilter, HttpException } from '@nestjs/common';
import * as path from 'path';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
    catch(exception: HttpException, host: ArgumentsHost) {

        // grab the exception object
        let exceptionObj: any = exception.getResponse();

        console.log("----- exception ----\n", exceptionObj);

        const ctx = host.switchToHttp();
        const response = ctx.getResponse();
        const request = ctx.getRequest();

        // 404 is special since the front-end handles that
        if (exceptionObj.statusCode == 404) {
            response.sendFile(path.join(__dirname, '../public/index.html'));

        // otherwise return the status and the JSONified status error
        } else {
            response
                .status(exceptionObj.statusCode)
                .json(exceptionObj)
        }
    }
}
