import { HttpException, HttpStatus } from '@nestjs/common';

export class UnauthorizedException extends HttpException {
    constructor(msg?: string) {
        super({
            statusCode: HttpStatus.UNAUTHORIZED,
            error: msg || 'Unauthorized Access',
            timestamp: new Date().toISOString()
        }, HttpStatus.UNAUTHORIZED)
    }
}
