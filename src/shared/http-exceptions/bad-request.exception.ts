import { HttpException, HttpStatus } from '@nestjs/common';

export class BadRequestException extends HttpException {
    constructor(msg?: string) {
        super({
            statusCode: HttpStatus.BAD_REQUEST,
            error: msg || 'Bad Request',
            timestamp: new Date().toISOString()
        }, HttpStatus.BAD_REQUEST)
    }
}
