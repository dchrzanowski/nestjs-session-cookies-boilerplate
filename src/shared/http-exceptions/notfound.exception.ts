import { HttpException, HttpStatus } from '@nestjs/common';

export class NotFoundException extends HttpException {
    constructor(msg?: string) {
        super({
            statusCode: HttpStatus.NOT_FOUND,
            error: msg || 'Not Found',
            timestamp: new Date().toISOString()
        }, HttpStatus.NOT_FOUND)
    }
}
