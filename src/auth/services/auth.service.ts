import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { DbUserService } from '../../db/services/db-user.service';
import { User } from '../../db/entities/user.entity';
import { IUserLoginDetails } from '../../shared/interfaces/user-login-details.interface';
import { UnauthorizedException } from '../../shared/http-exceptions/unauthorized.exception';
import { ServerErrorException } from '../../shared/http-exceptions/servererror.exception';
import { UserRoles } from '../../shared/enums/user-roles.enum';
import { UserStatus } from '../../shared/enums/user-status.enum';

@Injectable()
export class AuthService {

    constructor(
        private readonly dbUserService: DbUserService,
    ) { }

    /**
     * Hash the user's password and attach a JWT token to the user
     * @param user User object
     */
    async hashPassword(user: User): Promise<User> {

        user.password = bcrypt.hashSync(user.password, 8);

        user = await this.dbUserService.save(user);

        return user;
    }

    /**
     * Check user's existence in the database and verify password
     * @param userDetails User login information
     */
    async loginUser(userDetails: IUserLoginDetails) {

        const user = await this.dbUserService.findOneValidation({ email: userDetails.email });

        if (!user)
            throw new UnauthorizedException();

        const isAuthenticated = bcrypt.compareSync(userDetails.password, user.password);

        // create a new token if the user is valid
        if (isAuthenticated)
            return user;

        throw new UnauthorizedException();
    }

    /**
     * Create a new user account, hash the password and save the user in db
     * @param user User object
     */
    async createUser(user: User) {

        // new user cannot have an ID
        if (user.id)
            throw new ServerErrorException("User cannot have an ID");

        user.role = UserRoles.USER;
        user.status = UserStatus.ACTIVE;

        user = await this.hashPassword(user); // saves the user into db as well

        return user;
    }
}
