import { Injectable, ExecutionContext, CanActivate } from "@nestjs/common";
import { UnauthorizedException } from "../../shared/http-exceptions/unauthorized.exception";

@Injectable()
export class SessionGuard implements CanActivate {
    async canActivate(context: ExecutionContext): Promise<boolean> {
        const httpContext = context.switchToHttp();
        const request = httpContext.getRequest();

        if (request.user)
            return true;

        throw new UnauthorizedException();
    }
}
