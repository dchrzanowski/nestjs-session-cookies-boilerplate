import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';
import { UnauthorizedException } from '../../shared/http-exceptions/unauthorized.exception';

@Injectable()
export class RolesGuard implements CanActivate {

    constructor(private readonly reflector: Reflector) { }

    canActivate(
        context: ExecutionContext,
    ): boolean | Promise<boolean> | Observable<boolean> {

        // grab the roles from the decorator
        const roles = this.reflector.get<string[]>('roles', context.getHandler());

        if (!roles) {
            return true;
        }

        const request = context.switchToHttp().getRequest();
        const user = request.user;
        const hasRole = () => roles.includes(user.role);

        let isAuthorized: boolean = user && user.role && hasRole();

        if (isAuthorized) {
            return true;
        }

        throw new UnauthorizedException();
    }
}
