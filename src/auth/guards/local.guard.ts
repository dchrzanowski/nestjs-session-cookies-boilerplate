import { Injectable, ExecutionContext } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";

@Injectable()
export class LocalAuthGuard extends AuthGuard('local') {
    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();

        // Super canActivate() must establish an authenticated user before logIn() will work
        const can: boolean = await super.canActivate(context) as boolean;

        if (can)
            await super.logIn(request);

        return can;
    }
}
