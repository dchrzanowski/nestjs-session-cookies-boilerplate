import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DbModule } from '../db/db.module';
import { User } from '../db/entities/user.entity';
import { LocalAuthGuard } from './guards/local.guard';
import { AuthService } from './services/auth.service';
import { LocalSerializer } from './strategies/local.serializer';
import { LocalStrategy } from './strategies/local.strategy';
import { RolesGuard } from './guards/roles.guard';

@Module({
    imports: [
        PassportModule.register({
            defaultStrategy: 'local',
            session: true
        }),
        TypeOrmModule.forFeature([
            User,
        ]),
        DbModule,
    ],
    providers: [
        AuthService,
        LocalAuthGuard,
        LocalStrategy,
        LocalSerializer,
        RolesGuard,
    ],
    exports: [
        AuthService,
        LocalAuthGuard,
        RolesGuard,
    ]
})
export class AuthModule { }
