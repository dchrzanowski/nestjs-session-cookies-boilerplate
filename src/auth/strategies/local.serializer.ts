import { Injectable } from '@nestjs/common';
import { PassportSerializer } from '@nestjs/passport';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../../db/entities/user.entity';

@Injectable()
export class LocalSerializer extends PassportSerializer {
    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
    ) {
        super();
    }

    serializeUser(user: User, done: CallableFunction) {
        done(null, user.id);
    }

    async deserializeUser(userId: string, done: CallableFunction) {
        return await this.userRepository.findOneOrFail({ id: Number(userId) })
            .then(user => done(null, user))
            .catch(error => done(error));
    }
}
