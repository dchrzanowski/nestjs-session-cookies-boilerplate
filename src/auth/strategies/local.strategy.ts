import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';
import { AuthService } from '../services/auth.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
    constructor(
        private readonly authService: AuthService,
    ) {
        super();
    }

    async validate(email: string, password: string, done: CallableFunction) {

        return await this.authService.loginUser({ email: email, password: password })
            .then(user => done(null, user))
            .catch(error => done(error));
    }
}
