import { Module } from '@nestjs/common';
import { ControllersModule } from './controllers/controllers.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ResourceModule } from './resources/resource.module';

@Module({
    imports: [
        ResourceModule,
        TypeOrmModule.forRoot({
            type: "mysql",
            host: process.env.DB_HOST,
            port: +process.env.DB_PORT,
            username: process.env.DB_USER,
            password: process.env.DB_PASS,
            database: process.env.DB_NAME,
            entities: [`${__dirname}/**/*.entity{.ts,.js}`],
            synchronize: true
        }),
        ControllersModule,
    ]
})
export class AppModule { }
