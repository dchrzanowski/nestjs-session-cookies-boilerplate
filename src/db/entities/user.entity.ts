import { Column, Entity, PrimaryGeneratedColumn, Unique } from 'typeorm';

@Entity()
@Unique(['email'])
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 255 })
    name: string;

    @Column({ length: 191 })
    email: string;

    @Column({ type: 'text', select: false })
    password: string;

    @Column({ length: 10 })
    role: string;

    @Column({ length: 20 })
    status: string;

    @Column({ length: 255 })
    stripeTok: string;
}
