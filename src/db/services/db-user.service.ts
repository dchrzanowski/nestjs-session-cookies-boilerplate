import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../entities/user.entity';
import { ServerErrorException } from '../../shared/http-exceptions/servererror.exception';

@Injectable()
export class DbUserService {

    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
    ) { }

    /**
     * Find all users by query
     * @param query The query object (optional)
     * @throws ServerErrorException Server Error Exception
     */
    async find(query?: any): Promise<User[]> {

        let users: User[];

        try {
            users = await this.userRepository.find(query);
        } catch (e) {
            throw new ServerErrorException(e);
        }

        return users;
    }

    /**
     * Find a user by query
     * @param query The query object
     * @throws ServerErrorException Server Error Exception
     */
    async findOne(query: any): Promise<User> {

        let user: User;

        try {
            user = await this.userRepository.findOne(query);
        } catch (e) {
            throw new ServerErrorException(e);
        }

        return user;
    }

    /**
     * Save a user into the database
     * @param user User object
     * @throws ServerErrorException Server Error Exception
     */
    async save(user: User): Promise<User> {

        try {
            user = await this.userRepository.save(user);
        } catch (e) {
            throw new ServerErrorException(e);
        }

        return user;
    }

    /**
     * Find a user with its password
     * @param query The query object (optional)
     * @throws ServerErrorException Server Error Exception
     */
    async findOneValidation(query: any): Promise<User> {

        return await this.findOne({
            select: ['id', 'name', 'email', 'password', 'role'],
            where: query
        })
    }
}
