import { Global, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { DbUserService } from './services/db-user.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            User,
        ]),
    ],
    providers: [
        DbUserService,
    ],
    exports: [
        DbUserService,
    ]
})
export class DbModule { }
