import { Injectable } from '@nestjs/common';
import { ResourceStrings } from '../resources/strings.resource';
import { ResourceNums } from '../resources/num.resource';
import { ResourceArrays } from '../resources/array.resource';

@Injectable()
export class ResourceService {
    str: ResourceStrings;
    num: ResourceNums;
    arr: ResourceArrays;

    constructor() {
        this.str = new ResourceStrings();
        this.num = new ResourceNums();
        this.arr = new ResourceArrays();
    }
}
