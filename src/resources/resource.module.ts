import { Module, Global } from '@nestjs/common';
import { ResourceService } from './service/resources.service';

@Global()
@Module({
    providers: [
        ResourceService
    ],
    exports: [
        ResourceService
    ]
})
export class ResourceModule { }
