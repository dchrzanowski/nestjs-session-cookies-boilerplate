import { Controller, Get, Query, UseGuards, Post, Body, Request } from '@nestjs/common';
import { LocalAuthGuard } from '../../auth/guards/local.guard';
import { IUserLoginDetails } from '../../shared/interfaces/user-login-details.interface';
import { AuthService } from '../../auth/services/auth.service';
import { User } from '../../db/entities/user.entity';
import { SessionGuard } from '../../auth/guards/session.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { Roles } from 'src/auth/decorator/roles.decorator';
import { UserRoles } from 'src/shared/enums/user-roles.enum';

@Controller('api/user')
export class UserController {

    constructor(
        private readonly authService: AuthService,
    ) { }

    @Post("/login")
    @UseGuards(new LocalAuthGuard())
    async login(@Body() body: IUserLoginDetails) {

        return "Logged In";
    }

    @Post("/signup")
    async signup(@Body() body: User) {

        const user = await this.authService.createUser(body);
        return user;
    }

    @Post("/logout")
    async logout(@Request() req) {
        req.logout();
        return;
    }

    @Get('/protected')
    @UseGuards(new SessionGuard())
    async protectedPath() {
        return "Protected Reached";
    }

    @Get('/unprotected')
    async unprotectedPath() {
        return "Not Protected";
    }

    @Get('/admin-only')
    @UseGuards(new SessionGuard(), RolesGuard)
    @Roles(UserRoles.ADMIN)
    async adminOnly() {
        return "Admin only";
    }

    @Get('/user-only')
    @UseGuards(new SessionGuard(), RolesGuard)
    @Roles(UserRoles.USER)
    async userOnly() {
        return "User only";
    }

    @Get('/user-n-admin-only')
    @UseGuards(new SessionGuard(), RolesGuard)
    @Roles(UserRoles.USER, UserRoles.ADMIN)
    async usersAndAdminsOnly() {
        return "User and admins only";
    }
}
