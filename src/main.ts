import { join } from 'path';
import * as dotenv from 'dotenv';

// setup ENV before anything else is loaded (especially AppModule that relies on ENV vars)
const ENV = process.env.NODE_ENV ? process.env.NODE_ENV : 'dev';
dotenv.config({ path: join(__dirname, `../${ENV}.env`) });

import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { NestExpressApplication } from '@nestjs/platform-express';
import * as cookieParser from 'cookie-parser';
import * as compression from 'compression';
import * as session from 'express-session';
import * as passport from 'passport';
import * as MySQLStore from 'express-mysql-session';

async function bootstrap() {
    const port = process.env.PORT || 3000;
    const publicPath = join(__dirname, '../public');
    const app = await NestFactory.create<NestExpressApplication>(AppModule);
    const cookieAge = 14 * 24 * 60 * 60 * 1000;  // two weeks
    const sessionStore = new MySQLStore({
        host: process.env.DB_HOST,
        port: +process.env.DB_PORT,
        user: process.env.DB_USER,
        password: process.env.DB_PASS,
        database: process.env.DB_NAME
    });
    const sessionOpts = {
        store: sessionStore,
        secret: "someWeakSecret",
        cookie: { maxAge: cookieAge },
        saveUninitialized: false,
        resave: false
    };

    app.use(compression());

    app.use(cookieParser());

    app.use(session(sessionOpts))

    app.use(passport.initialize());

    app.use(passport.session());

    app.useStaticAssets(publicPath);

    app.listen(port).then(() => {
        console.log(`\n--- Server running on http://localhost:${port} ---\n`);
    })
}
bootstrap();
